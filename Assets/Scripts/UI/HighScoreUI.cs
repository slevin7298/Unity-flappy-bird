﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreUI : MonoBehaviour {
	Text HighScoreText;

	void Start () {
		HighScoreText = GetComponent<Text> ();
	}


	void Update () {
		HighScoreText.text = "HighScore:" + GameControl.instance.HighScore;
	}
}
