﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ColumnPoolModif : MonoBehaviour {
	#region Inner classes
	[System.Serializable]
	public class DifficultyConfig {
		public int Score;
		public SpawnConfig Config;
	}

	public int columnPoolSize = 5;
	public GameObject columnPrefab;

	private GameObject[] columns;
	private int currentColumn = 0;


	public List<DifficultyConfig> DifficultyLevels;
	#endregion


	[System.Serializable]
	public class SpawnConfig {

		[Range(0.5f, 2.5f)]
		public float ScrollsSpeed = 1;
		// Scroll speed of the game
	
		public float StartX = 5;
		// Spawn position X of the first column
	
		public float SpawnMin = 4;
		// Min spawn pos X of the next column

		public float SpawnMax = 7;
		// Max spawn pos X of the next column

		public float HMin = -1;
		// Min spawn pos Y of the next column

		public float Hmax = 3.5f;
		// Max spawn pos Y of the 

		public float SpacingMin =2;

		public float SpacingMax = 3.5f;

		[Range(0, 1)]
		public float PrctDouble = 0.5f;
	}
		
	// Use this for initialization
	void Start ()
	{
		columns = new GameObject[columnPoolSize];
		for (int i = 0; i < columnPoolSize; i++)
		{
			columns [i] = Instantiate (columnPrefab);
			columns [i].SetActive (false);
		}	

		SpawnColumn(DifficultyLevels[0].Config, 0);
	}

	void SpawnColumn(SpawnConfig config, float lastColumnPositionX) {
		// Position de la derniere colonneen x
		var lastColumnPosX = columns[currentColumn].transform.position.x;

		// Case du tableau qui contient la colonne a positionner et a activer
		var nextColumn = currentColumn + 1 >= columnPoolSize ? 0 : currentColumn + 1;

		// on récupère la colonne
		var column = columns[nextColumn];
		// On l'active
		column.SetActive(true);

		Vector3 position = new Vector3(lastColumnPositionX, 0, 0);


		position.x += Random.Range (config.SpawnMin, config.SpawnMax);
		position.y += Random.Range (config.HMin, config.Hmax);

		column.transform.position = position;


		bool IsDouble = Random.Range (0f, 1f) < config.PrctDouble;

		foreach (Transform child in column.transform) {
			child.gameObject.SetActive (true);
		}

		if (!IsDouble) {
			var childCount = column.transform.childCount;
			var hidePart = column.transform.GetChild(Random.Range(0, childCount));
			//Cacher l'enfant
			hidePart.gameObject.SetActive(false);
		}

		// Espacement entre les colonnes
		var spacing = Random.Range(config.SpacingMin, config.SpacingMax);
		column.transform.GetChild (0).localPosition = Vector3.down * spacing * 0.5f;
		column.transform.GetChild (1).localPosition = Vector3.up * spacing * 0.5f;

		currentColumn = nextColumn;
	}
		
	void Update () {
		if (columns[currentColumn].transform.position.x <= 8) {
			var difficulty = DifficultyLevels.OrderByDescending (c => c.Score)
				.OrderByDescending(c => c.Score)
				.FirstOrDefault (c => c.Score <= GameControl.instance.Score);
	
			SpawnColumn (difficulty.Config, columns [currentColumn].transform.position.x);
		}
	}
}
